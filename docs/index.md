n3t Login Redirect
==================

n3t Login Redirect is user plugin enabling to redirect users after login per their
user group.

Installation
------------

n3t Login Redirect is Joomla! user plugin. It could be installed as any other extension in
Joomla!

After installing **do not forget to enable the plugin** from Plugin Manager in your
Joomla! installation.