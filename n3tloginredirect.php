<?php
/**
 * @package n3t Login Redirect
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2019 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;

class plgUserN3tLoginRedirect extends JPlugin
{

  public function onUserAfterLogin($options = array())
  {
    $user = Factory::getUser();

    if ($userGroupRedirects = $this->params->get('usergroup_redirects')) {
      $userGroups = $user->getAuthorisedGroups();
      foreach ($userGroupRedirects as $redirect) {
        if (in_array($redirect->usergroup, $userGroups)) {
          Factory::getApplication()->redirect(Route::_('index.php?Itemid=' . $redirect->menuitem, false));
        }
      }
    }
  }

}
